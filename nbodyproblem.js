var g = 10; //gravitational constant
var n = 2; //initial number of bodies
//var density = 0.238732415/100 //body density
var newx;
var newy;
var newm = 0;
var dt = 0.001;
WrapModesEnum = Object.freeze({
    DESTROY : 0,
    BOUNCE : 1,
    WRAP_TORUS : 2,
	WRAP_MOB_HOR : 3,
	WRAP_MOB_VER : 4,
	WRAP_KLEIN : 5
})
var wrapMode = WrapModesEnum.DESTROY;
SpawnModesEnum = Object.freeze({
	CLICK : 0,
	AUTO: 1 //TODO
})
var spawnMode = SpawnModesEnum.CLICK;
CollisionModesEnum = Object.freeze({
	ELASTIC : 0,
	INELASTIC : 1,
	PHASE_THROUGH : 2
})
var collisionMode = CollisionModesEnum.PHASE_THROUGH;
ColorModesEnum = Object.freeze({
	NONE : 0,
	RANDOM : 1,
	MASS : 2,
	SPEED : 3
})
var colorType = ColorModesEnum.RANDOM;

function defineColor(b) {
	switch (colorType) {
		case (ColorModesEnum.NONE) :
			c = document.getElementById("backgroundColor").value;
			return color(300-hue(c), brightness(c), saturation(c));
			break;
		case (ColorModesEnum.RANDOM) :
			return color(Math.floor(Math.random() * 301), 100, 100);
			break;
		case (ColorModesEnum.MASS) :
			console.log("b.m = " + b.m);
			return color(300*(1-Math.pow(2,-b.m/50)), 100, 100);
			break;
		case (ColorModesEnum.SPEED) :
			k = Math.sqrt(b.vx*b.vx + b.vy*b.vy);
			console.log("speed = " + k);
			return color(300*(1-Math.pow(2,-k/10)), 100, 100);
			break;
	}
}

function body (x, y, vx, vy, m) {
	this.x = x;
	this.y = y;
	this.vx = vx;
	this.vy = vy;
	this.m = m;
	//this.r = Math.cbrt(0.238732415*m/density);
	this.r = Math.cbrt(100*m);
	this.color = defineColor(this);
}

function distance (a, b) {
	return Math.sqrt((a.x-b.x)*(a.x-b.x) + (a.y-b.y)*(a.y-b.y));
}

function setup() {

	//console.log("setup");
	createCanvas(1000, 600);

	colorMode(HSB);

	bodies = new Set();
	for (i = 0; i < n; i++) {
		bodies.add(new body (width/2, height/2 + 100*i, 1-2*i, 0, 10/(i+1)));
	}

}

function draw() {

	wrapMode = parseInt(document.getElementById("wrapMode").value);
	spawnMode = parseInt(document.getElementById("spawnMode").value);
	collisionMode = parseInt(document.getElementById("collisionMode").value);
	colorType = parseInt(document.getElementById("colorMode").value);
	background(document.getElementById("backgroundColor").value);

	let t = 0;
	while (t < 1) {
		for (let b of bodies) {
			ax = 0;
			ay = 0;
			for (let a of bodies) {
				if (a != b) {
					//console.log("corpo diferente");
					d = distance (a, b);
					if (d <= a.r + b.r) {
						//console.log("collision");
						switch (collisionMode) {
							case (CollisionModesEnum.ELASTIC) :
								do {
									b.x -= b.vx*dt/10;
									b.y -= b.vy*dt/10;
									a.x -= a.vx*dt/10;
									a.y -= a.vy*dt/10;
								} while (distance (a, b) <= a.r + b.r)
								//console.log("calculating speeds for elastic collision");
								avtemp = (2*b.m*b.vx + (a.m - b.m)*a.vx)/(a.m+b.m);
								bvtemp = (2*a.m*a.vx + (b.m - a.m)*b.vx)/(a.m + b.m);
								a.vx = avtemp;
								b.vx = bvtemp;
								avtemp = (2*b.m*b.vy + (a.m - b.m)*a.vy)/(a.m + b.m);
								bvtemp = (2*a.m*a.vy + (b.m - a.m)*b.vy)/(a.m + b.m);
								a.vy = avtemp;
								b.vy = bvtemp;
								break;
							case (CollisionModesEnum.INELASTIC) :
								b.m += a.m;
								//b.r = Math.cbrt(0.238732415*b.m/density);
								b.r = Math.cbrt(100*b.m);
								b.color = defineColor(b);
								b.vx = (b.m*b.vx + a.m*a.vx)/(b.m+a.m);
								b.vy = (b.m*b.vy + a.m*a.vy)/(b.m+a.m);
								bodies.delete(a);
								break;
							case (CollisionModesEnum.PHASE_THROUGH) :
								ax += g*a.m*(a.x-b.x)/(a.r*a.r*a.r);
								ay += g*a.m*(a.y-b.y)/(a.r*a.r*a.r);
								break;
						}
					} else {
						//console.log("distancia = " + d);
						ax += g*a.m*(a.x-b.x)/(d*d*d);
						//console.log("ax = " + ax);
						ay += g*a.m*(a.y-b.y)/(d*d*d);
						//console.log("ay = " + ay);
					}
				}
			}
			b.vx += ax*dt;
			//console.log("vx = " + b.vx);
			b.vy += ay*dt;
			//console.log("vy = " + b.vy);
			b.x += b.vx*dt;
			//console.log("x = " + b.x);
			b.y += b.vy*dt;
			//console.log("y = " + b.y);
			switch (wrapMode) {
				case (WrapModesEnum.DESTROY) :
					if (b.x < 0 || b.x > width || b.y < 0 || b.y > height) {
						bodies.delete(b);
					}
					break;
				case (WrapModesEnum.BOUNCE) :
					if (b.x < 0) {
						b.x = 0;
						b.vx *= -1;;
					}
					if (b.x > width) {
						b.x = width;
						b.vx *= -1;;
					}
					if (b.y < 0) {
						b.y = 0;
						b.vy *= -1;;
					}
					if (b.y > height) {
						b.y = height;
						b.vy *= -1;;
					}
					break;
				case (WrapModesEnum.WRAP_TORUS) :
					while (b.x < 0) {
						b.x += width;
					}
					while (b.x > width) {
						b.x -= width;
					}
					while (b.y < 0) {
						b.y += height;
					}
					while (b.y > height) {
						b.y -= height;
					}
					break;
				case (WrapModesEnum.WRAP_MOB_HOR) :
					while (b.x < 0) {
						b.x += width;
						b.y = height - b.y;
						b.vy = -b.vy;
					}
					while (b.x > width) {
						b.x -= width;
						b.y = height - b.y;
						b.vy = -b.vy;
					}
					while (b.y < 0) {
						b.y += height;
					}
					while (b.y > height) {
						b.y -= height;
					}
					break;
				case (WrapModesEnum.WRAP_MOB_VER) :
					while (b.x < 0) {
						b.x += width;
					}
					while (b.x > width) {
						b.x -= width;
					}
					while (b.y < 0) {
						b.y += height;
						b.x = width - b.x;
						b.vx = -b.vx;
					}
					while (b.y > height) {
						b.y -= height;
						b.x = width - b.x;
						b.vx = -b.vx;
					}
					break;
				case (WrapModesEnum.WRAP_KLEIN) :
					while (b.x < 0) {
						b.x += width;
						b.y = height - b.y;
						b.vy = -b.vy;
					}
					while (b.x > width) {
						b.x -= width;
						b.y = height - b.y;
						b.vy = -b.vy;
					}
					while (b.y < 0) {
						b.y += height;
						b.x = width - b.x;
						b.vx = -b.vx;
					}
					while (b.y > height) {
						b.y -= height;
						b.x = width - b.x;
						b.vx = -b.vx;
					}
					break;
			}
		}
		t += dt;
	}
	for (let b of bodies) {
		if (colorType != ColorModesEnum.RANDOM) {
			b.color = defineColor(b);
		}
		noStroke();
		fill(b.color);
		ellipse(b.x,b.y,2*b.r,2*b.r);
	}

	if (mouseIsPressed && spawnMode == SpawnModesEnum.CLICK) {
		newm += .3;
		noStroke();
		fill(128);
		//newr = Math.cbrt(0.238732415*newm/density);
		newr = Math.cbrt(100*newm);
		ellipse(newx, newy, 2*newr, 2*newr);
		stroke(255);
		line(newx, newy, mouseX, mouseY);
	}

	if (frameCount % 180 == 0 && spawnMode == SpawnModesEnum.AUTO) {
		if (bodies.size < 20) {
			bodies.add(new body(
				Math.floor(Math.random() * width),
				Math.floor(Math.random() * height),
				Math.floor(Math.random() * width/100),
				Math.floor(Math.random() * height/100),
				Math.floor(Math.random() * 100 + 50)
			));
		}
	}

}

function keyPressed() {
	setup();
}

function mousePressed() {
	newx = mouseX;
	newy = mouseY;
}

function mouseReleased() {
	if (newm > 0 && newx > 0 && newx < width && newy > 0 && newy < height) {
		bodies.add(new body(newx, newy, .01*(mouseX-newx), .01*(mouseY-newy), newm));
	}
	newm = 0;
}
